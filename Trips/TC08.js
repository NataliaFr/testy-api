pm.test('TC08: directionId are equal to 1 or 0', function () {

    const trips = pm.response.json();

    pm.expect(trips.every((trip) => {
        return trip.directionId == 0 || trip.directionId == 1;
    })).to.be.true;

});