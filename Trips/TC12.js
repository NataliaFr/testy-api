pm.test('TC12: bikesAllowed are null ', function () {

    const trips = pm.response.json();

    pm.expect(trips.every((trip) => {
        return trip.bikesAllowed == null;
    })).to.be.true;

});