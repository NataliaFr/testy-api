var schema = {
    "items": {
        "properties": {
            "tripId": {
                "type": "integer"
            }
        }
    }
}

pm.test('TC05: tripId  is integer', function () {

    var result = tv4.validateResult(JSON.parse(responseBody), schema);

    pm.expect(result.valid).to.be.true;
})