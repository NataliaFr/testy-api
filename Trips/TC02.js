pm.test('TC02: routeId is non negative ', function () {

    const trips = pm.response.json();

    pm.expect(trips.every((trip) => {
        return trip.routeId > 0;
    })).to.be.true;

});