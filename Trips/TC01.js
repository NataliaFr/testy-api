var schema = {
    "items": {
        "properties": {
            "routeId": {
                "type": "integer"
            }
        }
    }
}

pm.test('TC01: check if All routeId  are integer', function () {

    var result = tv4.validateResult(JSON.parse(responseBody), schema);

    pm.expect(result.valid).to.be.true;
})