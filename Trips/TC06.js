pm.test('TC06: tripHeadsign are not empty ', function () {

    const trips = pm.response.json();

    pm.expect(trips.every((trip) => {
        return trip.tripHeadsign !== null;
    })).to.be.true;

});