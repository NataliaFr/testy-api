pm.test('TC11: weelchairAccessible are empty ', function () {

    const trips = pm.response.json();

    pm.expect(trips.every((trip) => {
        return trip.weelchairAccessible == null;
    })).to.be.true;

});