var schema = {
    "items": {
        "properties": {
            "serviceId": {
                "type": "integer"
            }
        }
    }
}

//Validate Schema
pm.test('TC03: check if All serviceId   are integer', function () {

    var result = tv4.validateResult(JSON.parse(responseBody), schema);

    pm.expect(result.valid).to.be.true;
})