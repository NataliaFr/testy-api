pm.test('TC09 : tripShortName are  empty ', function () {

    const trips = pm.response.json();

    pm.expect(trips.every((trip) => {
        return trip.tripShortName !== null;
    })).to.be.true;

});