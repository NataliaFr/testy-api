var schema = {
    "items": {
        "properties": {
            "serviceId": {
                "type": "string"
            }
        }
    }
}

pm.test('TC04: check if All serviceId   are string', function () {
    var result = tv4.validateResult(JSON.parse(responseBody), schema);
    pm.expect(result.valid).to.be.true;
})