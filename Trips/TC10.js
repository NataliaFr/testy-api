pm.test('TC10: shapeId are empty', function () {

    const trips = pm.response.json();

    pm.expect(trips.every((trip) => {
        return trip.shapeId === null;
    })).to.be.true;

});