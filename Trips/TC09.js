pm.test('TC09: blockId are empty ', function () {

    const trips = pm.response.json();

    pm.expect(trips.every((trip) => {
        return trip.blockId == null;
    })).to.be.true;

});