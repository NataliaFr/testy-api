var schema = {
    "items": {
        "properties": {
            "routeShortName": {
                "type": "string"
            }
        }
    }
}

pm.test('TC04 : routeShortName  is  a strings of characters', function () {

    var result = tv4.validateResult(JSON.parse(responseBody), schema);
    pm.expect(result.valid).to.be.true;

})