pm.test('TC13 : routeSortOrder are empty ', function () {

    const routes = pm.response.json();

    pm.expect(routes.every((route) => {
        return route.routeSortOrder == null;
    })).to.be.true;

});
