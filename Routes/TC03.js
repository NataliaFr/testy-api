pm.test('TC03 : agency_id  is greater then or equal 0', function () {

    const routes = pm.response.json();

    pm.expect(routes.every((route) => {
        return route.agencyId >= 0;
    })).to.be.true;

});