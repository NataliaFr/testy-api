
pm.test('TC06 : routeDesc are empty ', function () {

	const routes = pm.response.json();

    pm.expect(routes.every((route) => {
        return route.routeDesc === null;
    })).to.be.true;

});