pm.test('TC10 : routeURL are empty ', function () {


    const routes = pm.response.json();

    pm.expect(routes.every((route) => {
        return route.routeURL == null;
    })).to.be.true;

});