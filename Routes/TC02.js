pm.test('TC02 : routeId is non-negative Integer', function () {

    const routes = pm.response.json();

    pm.expect(routes.every((route) => {
        return route.routeId > 0;
    })).to.be.true;

});