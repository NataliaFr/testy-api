pm.test('TC11 : routeColor are empty ', function () {

    const routes = pm.response.json();

    pm.expect(routes.every((route) => {
        return route.routeColor == null;
    })).to.be.true;

});