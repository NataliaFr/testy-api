var schema = {
    "items": {
        "properties": {
            "routeLongName": {
                "type": "string"
            }
        }
    }
}

//Validate Schema
pm.test('TC05: routeLongName is  a strings of characters', function () {

    var result = tv4.validateResult(JSON.parse(responseBody), schema);
    pm.expect(result.valid).to.be.true;

})