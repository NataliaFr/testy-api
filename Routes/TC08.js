
pm.test('TC08 : Value  routeType : 800', function () {

    const routes = pm.response.json();

    var routesFilterArray = routes.filter(function (route) {
        return route.routeType == 800;
    });

    pm.expect(routesFilterArray.length > 0).to.be.true;

});