pm.test('TC07 :  Value  routeType : 700', function () {

    const routes = pm.response.json();

    var routesFilterArray = routes.filter(function (route) {
        return route.routeType == 700;
    });

    pm.expect(routesFilterArray.length > 0).to.be.true;

});