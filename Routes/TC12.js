pm.test('TC12 : routeTextColor are empty ', function () {

    const routes = pm.response.json();

    pm.expect(routes.every((route) => {
        return route.routeTextColor == null;
    })).to.be.true;

});
