pm.test('TC013: parentStation are empty ', function () {

    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        return stop.parentStation === null;
    })).to.be.true;

});