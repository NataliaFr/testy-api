pm.test('TC05 : stopName are not  empty ', function () {


    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        return stop.stopName !== null;
    })).to.be.true;

});