var schema = {
    "items": {
        "properties": {
            "stopName": {
                "type": "string"
            }
        }
    }
}

//Validate Schema
pm.test('TC06: check if stopName  is string', function () {

    var result = tv4.validateResult(JSON.parse(responseBody), schema);
    pm.expect(result.valid).to.be.true;

})