pm.test('TC08 :stopLon is longitude in decimal degrees. The value is greater than or equal to -180.0 and less than or equal to 180.0.  ', function () {

    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        var stopLon = parseFloat(stop.stopLon);
        return stopLon <= 180 && stopLon >= -180;
    })).to.be.true;

});