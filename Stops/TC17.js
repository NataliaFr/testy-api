pm.test('TC017: ticketZoneBorder equal to 0 or 1 ', function () {

    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        return stop.ticketZoneBorder == 0 || stop.ticketZoneBorder == 1;
    })).to.be.true;

});