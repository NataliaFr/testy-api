pm.test('TC02 : stopId is not null', function () {

    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        return stop.stopId !== null;
    })).to.be.true;

});