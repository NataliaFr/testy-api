pm.test('TC12: locationType are empty ', function () {

    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        return stop.locationType === null;
    })).to.be.true;

});