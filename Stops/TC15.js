pm.test('TC15 : stopTimezone with incorect value  stop_timezone:”Europe/London”', function () {

    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        return stop.stopTimezone == "Europe/London";
    })).to.be.true;

});