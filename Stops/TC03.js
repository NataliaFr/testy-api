var schema = {
    "items": {
        "properties": {
            "stopId": {
                "type": "integer"
            }
        }
    }
}

pm.test('TC03: check if stopId  is intrger', function () {

    var result = tv4.validateResult(JSON.parse(responseBody), schema);
    pm.expect(result.valid).to.be.true;

})


pm.test('TC03 : stop id < 0 ', function () {

    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        return stop.stopId < 0;
    })).to.be.true;

});
