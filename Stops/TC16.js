pm.test('TC016: wheelchairBoarding are empty ', function () {

    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        return stop.wheelchairBoarding === null;
    })).to.be.true;

});