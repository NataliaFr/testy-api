pm.test('TC04 : StopId are non negative value', function () {

    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        return stop.stopId > 0;
    })).to.be.true;

});