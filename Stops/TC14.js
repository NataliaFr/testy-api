pm.test('TC14 : stop_timezone is  value (Europe/Warsaw)', function () {

    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        return stop.stopTimezone == "Europe/Warsaw";
    })).to.be.true;

});