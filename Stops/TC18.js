pm.test('TC018: ticketZoneBorder are empty ', function () {

    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        return stop.ticketZoneBorder === null;
    })).to.be.true;

});