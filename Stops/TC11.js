pm.test('TC011: stopURL are empty ', function () {

    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        return stop.stopURL === null;
    })).to.be.true;

});