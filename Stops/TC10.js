pm.test('TC010: zoneId  are not empty ', function () {

    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        return stop.zoneId !== null;
    })).to.be.true;

});