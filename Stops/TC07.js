pm.test('TC07 : stop_lat is geographical coordinate. The value is greater than or equal to -90.0 and less than or equal to 90.0. ', function () {

    const stops = pm.response.json();

    pm.expect(stops.every((stop) => {
        var stopLat = parseFloat(stop.stopLat);
        return stopLat <= 90 && stopLat >= -90;
    })).to.be.true;

});