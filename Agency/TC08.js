pm.test('TC08: agency_phone are string of letters', function () {

    const agencies = pm.response.json();

    pm.expect(agencies.every((agency) => {
        return /^[a-zA-Z]+$/.test(agency.agencyPhone);
    })).to.be.true;

});