pm.test('TC06: agency_lang code are equal to pl', function () {

    const agencies = pm.response.json();

    pm.expect(agencies.every((agency) => {
        return agency.agencyLang == 'pl';
    })).to.be.true;

});