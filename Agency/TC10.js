function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}


pm.test('TC10: Check if  is agencyEmail is an e-mail adress format', function () {

    const agencies = pm.response.json();

    pm.expect(agencies.every((agency) => {
        var email = agency.agencyEmail;
        return validateEmail(email);
    })).to.be.true;

});